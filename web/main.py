from flask import Flask, render_template, jsonify
from random import sample
import datetime
from config import DevConfig
from sql_acess import getData
from env import ENV

app = Flask(__name__)
app.config.from_object(DevConfig)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/data')
def data():
    vs1, ts1, vs2, ts2 = getData()
    ts1 = [t.strftime("%H:%M:%S") for t in ts1]
    ts2 = [t.strftime("%H:%M:%S") for t in ts2]
    return jsonify({
        'v1': vs1,
        't1': ts1,
        'v2': vs2,
        't2': ts2
    })

if __name__ == '__main__':
    app.run(
        host=ENV['HTTP']['HOST'],
        port=ENV['HTTP']['PORT']
    )
