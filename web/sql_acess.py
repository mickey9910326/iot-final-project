import pyodbc
import datetime
from env import ENV

__all__ = ['getData']


_S = "DRIVER={{{driver}}}; SERVER={host},{port}; DATABASE={db}; UID={uid}; PWD={pwd}"
_S = _S.format(
	driver=ENV['DB']['DRIVER'],
	host=ENV['DB']['HOST'],
	port=ENV['DB']['PORT'],
	db=ENV['DB']['DATABASE'],
	uid=ENV['DB']['UID'],
	pwd=ENV['DB']['PWD'],
)

cnxn = pyodbc.connect(_S)
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setencoding(encoding='utf-8')

def getData():
	sql = """
	SELECT *
		FROM (SELECT *
			FROM public.volt
			WHERE id = 1
			ORDER BY t DESC
			LIMIT 300) as q
		ORDER BY t
	"""
	
	sql2 = """
	SELECT *
		FROM (SELECT *
			FROM public.volt
			WHERE id = 2
			ORDER BY t DESC
			LIMIT 300) as q
		ORDER BY t
	"""

	cursor = cnxn.cursor()
	cursor.execute(sql)
	res = cnxn.commit()
	rows = cursor.fetchall()
	v1 = [row.v for row in rows]
	t1 = [row.t for row in rows]

	cursor.execute(sql2)
	res = cnxn.commit()
	rows = cursor.fetchall()
	v2 = [row.v for row in rows]
	t2 = [row.t for row in rows]
	return v1, t1, v2, t2
